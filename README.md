# Questionnaire

Outil utilisé pour faire des questionnaires.

## Architecture

Basé sur le dépot [questionnaire](https://gitlab.mim-libre.fr/alphabet/questionnaire). Utilise la base Mongodb de laboite et *(pour le moment non implémenté)* compatible avec Keycloak.
